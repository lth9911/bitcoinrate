# README #


### Simple PubNub in Action Application ###

I made this simple project to display the current price of BitCoin in USD, GBP and EUR. The website will automatically update BitCoin price every 1 minute.


### How do I get set up? ###

The tools I used for this project include Django==1.9.5, pubnub==3.7.6 and CoinDesk API. See the requirements.txt for more details. 

The project has 2 parts. The first part is pub.py that will get the updated BitCoin price every 10 seconds and publish to channel 'Current Rate' via Pubnub. To get BitCoin price I used CoinDesk API which is free. The second part is Django which I used as server run the index page that display the BitCoin price. 

### How to run the project in Mac Terminal ###
I assume that you start from scratch
1. install pip using *sudo easy_install pip*
2. *git clone https://lth9911@bitbucket.org/lth9911/bitcoinrate.git*
3. *cd bitcoinrate*
4. *pip install -r requirements.txt*
5. *python pub.py*
6. Open new command tab, direct to folder src *cd src*
7. *python manage.py runserver*
8. Open a web browser and direct to http:127.0.0.1:8000 or 8080

### Current State ###
The site does not have any CSS now, and I will work on it later as this project is to make a simple PubNub in Action app.