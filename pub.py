from pubnub import Pubnub
import time
import sys
import os
import urllib2, json
#
# run pub.py to get current price from CoinDesk.com
# publish current rate to PubNub
#
pubnub = None

def on_error(message):
	print "ERROR: " + str(message)

def publish_json_data():
	while True:
		url = "http://api.coindesk.com/v1/bpi/currentprice.json"
		response  = urllib2.urlopen(url)
		data = json.load(response)
	# for msg in data:
		print "publishg to %s : %s" % ("Current Rate channel", data)
		pubnub.publish(channel="Current Rate", message=data, error=on_error)
		time.sleep(10)
if __name__ == "__main__":
	# pub_key=os.environ['PUB_KEY']
	# sub_key=os.environ['SUB_KEY']
	pubnub = Pubnub(publish_key='pub-c-3a7e234c-15d9-4e47-a62d-c6a3d2fbb300', subscribe_key='sub-c-faf0f6b0-ffb7-11e5-b552-02ee2ddab7fe')
	publish_json_data()
